<!Doctype html>

<?php
	include_once('Includes/connection.php');
	include_once('Includes/article.php');

	$article = new Article;
	$articles = $article->fetch_all();

?>


<html>
	<head>
		<title>CMS Tutorial</title>
		<link rel="stylesheet" href="Assets/style.css" type="text/css" />
	</head>

	<body>
		<div class="container">
			<a href="index.php" id="logo">CMS</a>
			
			<ol>
				<?php foreach ($articles as $article) { ?>
				
				<li>
					<a href="article.php?id=<?php echo $article['article_id']; ?>">
						<?php echo $article['article_title'] ?>
					</a> 
					- <small>
						posted <?php echo date('l jS M Y', $article['article_timestamp']); ?>
					</small>
				</li>
				<?php } ?>
			</ol>

			<br />

			<small><a href="admin">Admin</a></small>
		</div>
	</body>
</html>
<!Doctype html>
<?php

	include_once('Includes/connection.php');
	include_once('Includes/article.php');

	$article = new Article;

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
		$data = $article->fetch_data($id);

?>

	<html>
		<head>
			<title>CMS Tutorial</title>
			<link rel="stylesheet" href="Assets/style.css" type="text/css" />
		</head>

		<body>
			<div class="container">
				<a href="index.php" id="logo">CMS</a>
				
				<h4>
					<?php echo $data['article_title']; ?>
				</h4>
				- <small>
						posted <?php echo date('l jS M Y', $data['article_timestamp']); ?>
				</small>

				<p>
					<?php echo $data['article_content']; ?>
				</p>

				<a href="index.php">&larr; Back</a>
			</div>
		</body>
	</html>
<?php
	} else {
		header('Location: index.php');
		exit();
	}

?>

